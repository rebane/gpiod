#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "json.h"
#include "socket_ud.h"
#include "gpiod.h"
#include "parse_mode.h"
#include "parse_pin.h"

int main(int argc, char *argv[]){
	json_object_t data, sets, set, mode, value, gpios, gpio;
	char buffer[65536], path[256];
	struct gpiod_gpio_struct g;
	int s, l, i, j;

	s = socket_ud_bind("/tmp/gpiod-socket");
	if(s < 0) {
		fprintf(stderr, "socket\n");
		exit(1);
	}

	while (1) {
		l = socket_ud_recvfrom(s, 0, buffer, 65536, path);
		buffer[l] = 0;
		printf("%s\n", buffer);
		if (json_value_isequal((unsigned char *)buffer, l, "command", "set")) {
			if (json_get_value((unsigned char *)buffer, l, "data", &data)) {
				if (json_get_value(data.start, data.len, "set", &sets) && (sets.type == JSON_TYPE_ARRAY)) {
					for (i = 0; i < sets.count; i++) {
						if (!json_get(sets.start, sets.len, i, NULL, &set) || (set.type != JSON_TYPE_MAP))
							continue;

						if (json_get_value(set.start, set.len, "mode", &mode) || (mode.type != JSON_TYPE_MAP)) {
							if (!parse_mode(&g.mode, mode.start, mode.len))
								continue;
						} else {
							parse_mode(&g.mode, "{}", 2);
						}

						if (json_get_value(set.start, set.len, "value", &value)) {
							if (value.type == JSON_TYPE_TRUE) {
								g.value = GPIOD_VALUE_TRUE;
							} else {
								g.value = GPIOD_VALUE_FALSE;
							}
						} else {
							g.value = GPIOD_VALUE_UNCHANGED;
						}

						if (!json_get_value(set.start, set.len, "gpios", &gpios) || (gpios.type != JSON_TYPE_ARRAY))
							continue;

						for (j = 0; j < gpios.count; j++) {
							if (!json_get(gpios.start, gpios.len, j, NULL, &gpio) || (gpio.type != JSON_TYPE_MAP))
								continue;

							if (!parse_pin(&g.pin, gpio.start, gpio.len))
								continue;

							gpiod_set(&g);
						}
					}
					socket_ud_sendto(s, 0, "{\"command\":\"ack\"}", 17, path);
					continue;
				}
			}
		}
		socket_ud_sendto(s, 0, "{\"command\":\"error\"}", 19, path);
	}
}

