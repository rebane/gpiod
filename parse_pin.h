#ifndef _PARSE_PIN_H_
#define _PARSE_PIN_H_

#include "gpiod.h"

int parse_pin(struct gpiod_pin_struct *pin, unsigned char *json_start, int json_len);

#endif

