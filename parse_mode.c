#include "parse_mode.h"
#include <unistd.h>
#include <stdlib.h>
#include "gpiod.h"
#include "json.h"

int parse_mode(struct gpiod_mode_struct *mode, unsigned char *json_start, int json_len)
{
	json_object_t value;

	if (json_value_isequal(json_start, json_len, "direction", "input")) {
		mode->dir = GPIOD_MODE_DIR_INPUT;
	} else if (json_value_isequal(json_start, json_len, "direction", "output")) {
		mode->dir = GPIOD_MODE_DIR_OUTPUT;
	} else {
		mode->dir = GPIOD_MODE_DIR_UNCHANGED;
	}
}

