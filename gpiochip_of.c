#include "gpiochip_of.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>

static int gpiochip_of_test_compatible_reg(char *gpiochip, int level, char *compatible, uint32_t reg);
static int gpiochip_of_test_compatible(char *path, char *compatible);
static int gpiochip_of_test_reg(char *path, uint32_t reg);

int gpiochip_of_get_arg(char *compatible, ...)
{
	struct gpiochip_of_struct *go;
	va_list arg;
	int count;
	int i;

	if (compatible == NULL)
		return -1;

	va_start(arg, compatible);
	va_arg(arg, uint32_t);
	for (count = 1; ; count++) {
		if (va_arg(arg, char *) == NULL)
			break;
		va_arg(arg, uint32_t);
	}
	va_end(arg);

	go = malloc(sizeof(*go) * count);
	if (go == NULL)
		return -1;

	va_start(arg, compatible);
	go[0].compatible = compatible;
	go[0].reg = va_arg(arg, uint32_t);
	for (i = 1; i < count; i++) {
		go[i].compatible = va_arg(arg, char *);
		go[i].reg = va_arg(arg, uint32_t);
	}
	va_end(arg);

	i = gpiochip_of_get(count, go);
	free(go);
	return i;
}

int gpiochip_of_get(int count, struct gpiochip_of_struct *go)
{
	struct dirent *dent;
	DIR *dir;
	int ret;
	int i;
	int c;

	dir = opendir("/sys/bus/gpio/devices");
	if (dir == NULL)
		return -1;

	ret = -1;
	while ((dent = readdir(dir)) != NULL) {
		if (sscanf(dent->d_name, "gpiochip%d", &c) != 1)
			continue;

		for (i = 0; i < count; i++) {
			if (!gpiochip_of_test_compatible_reg(dent->d_name, count - i - 1, go[i].compatible, go[i].reg))
				break;
		}
		if (i >= count) {
			ret = c;
			break;
		}
	}
	closedir(dir);
	return ret;
}

static int gpiochip_of_test_compatible_reg(char *gpiochip, int level, char *compatible, uint32_t reg)
{
	char buffer[1024];
	int i, l;

	l = 0;
	l += snprintf(&buffer[l], 1024 - l, "/sys/bus/gpio/devices/%s/of_node", gpiochip);
	for (i = 0; i < level; i++) {
		l += snprintf(&buffer[l], 1024 - l, "/..");
	}
	if(l > 1023)
		l = 1023;
	buffer[l] = 0;

	i = l + snprintf(&buffer[l], 1024 - l, "/compatible");
	if (i > 1023)
		i = 1023;
	buffer[i] = 0;
	if (!gpiochip_of_test_compatible(buffer, compatible))
		return 0;

	i = l + snprintf(&buffer[l], 1024 - l, "/reg");
	if (i > 1023)
		i = 1023;
	buffer[i] = 0;
	if (!gpiochip_of_test_reg(buffer, reg))
		return 0;

	return 1;
}

static int gpiochip_of_test_compatible(char *path, char *compatible)
{
	char buffer[1024];
	int f, l;

	f = open(path, O_RDONLY);
	if (f < 0)
		return 0;
	l = read(f, buffer, 1023);
	close(f);
	if (l < 0)
		return 0;
	buffer[l] = 0;
	if (strcmp(buffer, compatible))
		return 0;
	return 1;
}

static int gpiochip_of_test_reg(char *path, uint32_t reg)
{
	uint8_t buffer[4];
	int f, l;

	f = open(path, O_RDONLY);
	if (f < 0)
		return 0;
	l = read(f, buffer, 4);
	close(f);
	if (l != 4)
		return 0;
	if (((reg >> 24) & 0xFF) != buffer[0])
		return 0;
	if (((reg >> 16) & 0xFF) != buffer[1])
		return 0;
	if (((reg >> 8) & 0xFF) != buffer[2])
		return 0;
	if (((reg >> 0) & 0xFF) != buffer[3])
		return 0;
	return 1;
}

