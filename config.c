#include "config.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include "json.h"

static char *config_find_alias_file(int fd, char *alias, char *value);
static char *config_find_value(char *gpio, char *mode, json_object_t *values, char *value);
static char *config_json_dup(json_object_t *o);

char *config_find_alias(char *etc, char *alias, char *value)
{
	struct dirent *dent;
	char *filename;
	char *data;
	DIR *dir;
	int fd;

	dir = opendir(etc);
	if (dir == NULL)
		return NULL;

	data = NULL;

	while ((dent = readdir(dir)) != NULL) {
		if (asprintf(&filename, "%s/%s", etc, dent->d_name) < 0)
			continue;

		fd = open(filename, O_RDONLY);
		free(filename);
		if (fd < 0)
			continue;

		data = config_find_alias_file(fd, alias, value);
		close(fd);
		if (data != NULL)
			break;
	}
	closedir(dir);

	return data;
}

static char *config_find_alias_file(int fd, char *alias, char *value)
{
	json_object_t key, conf, gpio, mode, values;
	char *_gpio, *_mode, *data;
	char buffer[65536];
	int i, l;

	l = read(fd, buffer, 65536);
	if (l < 0)
		return NULL;

	for (i = 0; ; i++) {
		if (!json_get(buffer, l, i, &key, &conf))
			break;

		if (json_isequal(&key, alias)) {
			if (!json_get_value(conf.start, conf.len, "gpio", &gpio))
				return NULL;

			_gpio = config_json_dup(&gpio);
			if (_gpio == NULL)
				return NULL;

			if (json_get_value(conf.start, conf.len, "mode", &mode)) {
				_mode = config_json_dup(&mode);
				if (_mode == NULL) {
					free(_gpio);
					return NULL;
				}
			} else {
				_mode = NULL;
			}

			if (json_get_value(conf.start, conf.len, "values", &values))
				data = config_find_value(_gpio, _mode, &values, value);
			else
				data = config_find_value(_gpio, _mode, NULL, value);

			free(_gpio);
			if (_mode != NULL)
				free(_mode);

			return data;
		}
	}
	return NULL;
}

static char *config_find_value(char *gpio, char *mode, json_object_t *values, char *value)
{
	json_object_t key, state;
	char *s = NULL;
	char *conf;
	int i, ret;

	if (values != NULL) {
		for (i = 0; ; i++) {
			if (!json_get(values->start, values->len, i, &key, &state))
				break;

			if (json_isequal(&key, value)) {
				s = config_json_dup(&state);
				if (s == NULL)
					return NULL;
				break;
			}
		}
	}
	if (s == NULL) {
		if (!strcasecmp(value, "on") || !strcasecmp(value, "high") || !strcasecmp(value, "true") || !strcasecmp(value, "up")) {
			s = strdup("true");
		} else if (!strcasecmp(value, "off") || !strcasecmp(value, "low") || !strcasecmp(value, "false") || !strcasecmp(value, "down")) {
			s = strdup("false");
		}
	}
	if (s == NULL)
		return NULL;
	// '{"command":"set","data":{"set":[{"gpios":[{"type":"gpiochip","hint":"of","of":[{"compatible":"nuvoton,nuc980-gpio","reg":"0xb0004080"}],"pin":14}],"mode":{"direction":"output","value":false}

	if (mode != NULL) {
		ret = asprintf(&conf, "{\"command\":\"set\",\"data\":{\"set\":[{\"gpios\":[%s],\"mode\":%s,\"value\":%s}]}}", gpio, mode, s);
	} else {
		ret = asprintf(&conf, "{\"command\":\"set\",\"data\":{\"set\":[{\"gpios\":[%s],\"value\":%s}]}}", gpio, s);
	}
	free(s);
	if (ret < 0)
		return NULL;

	return conf;
}

static char *config_json_dup(json_object_t *o)
{
	char *s;
	s = malloc(o->len + 1);
	if (s == NULL)
		return NULL;

	memcpy(s, o->start, o->len);
	s[o->len] = 0;
	return s;
}

