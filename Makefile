CROSS ?= arm-linux-gnueabi-

all: server client
#	$(CROSS)gcc main.c gpiod_of.c

server:
	$(CROSS)gcc -Iinclude server.c json.c socket_ud.c gpiod.c parse_pin.c parse_mode.c gpiochip.c gpiochip_of.c -o gpiod

client:
	$(CROSS)gcc -D_GNU_SOURCE client.c json.c socket_ud.c config.c -o gpiodset

