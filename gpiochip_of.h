#ifndef _GPIOCHIP_OF_H_
#define _GPIOCHIP_OF_H_

#include <stdarg.h>
#include <stdint.h>

struct gpiochip_of_struct {
	char *compatible;
	uint32_t reg;
};

int gpiochip_of_get_arg(char *compatible, ...);
int gpiochip_of_get(int count, struct gpiochip_of_struct *go);

#endif

