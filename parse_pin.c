#include "parse_pin.h"
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include "gpiod.h"
#include "gpiochip.h"
#include "gpiochip_of.h"
#include "json.h"

static int parse_pin_gpiochip(struct gpiod_pin_struct *pin, unsigned char *json_start, int json_len);
static int parse_device(int *major, int *minor, char *device);

int parse_pin(struct gpiod_pin_struct *pin, unsigned char *json_start, int json_len)
{
	pin->chip_type = GPIOD_CHIP_TYPE_UNUSED;
	pin->device[0] = 0;

	if (json_value_isequal(json_start, json_len, "type", "gpio") || json_value_isequal(json_start, json_len, "type", "gpiochip")) {
		pin->chip_type = GPIOD_CHIP_TYPE_GPIOCHIP;
		return parse_pin_gpiochip(pin, json_start, json_len);
	}

	return 0;
}

static int parse_pin_gpiochip(struct gpiod_pin_struct *pin, unsigned char *json_start, int json_len)
{
	json_object_t type, hint_value, temp1, temp2;
	struct gpiochip_of_struct *gpiochip_of;
	int i, chip, ret;
	char *s;

	if (json_value_isequal(json_start, json_len, "hint", "name")) {
	} else if (json_value_isequal(json_start, json_len, "hint", "gpiochip")) {
		if (!json_get_value(json_start, json_len, "pin", &temp1))
			return 0;

		pin->pin = json_int(&temp1);

		if (!json_get_value(json_start, json_len, "gpiochip", &temp1))
			return 0;

		if (temp1.type == JSON_TYPE_STRING) {
			s = json_strdup(&temp1);
			if (s == NULL)
				return 0;

			if (!gpiochip_get_dev_str(s, &pin->chip_major, &pin->chip_minor)) {
				free(s);
				return 0;
			}
			snprintf(pin->device, 255, "/dev/%s", s);
			pin->device[255] = 0;
			free(s);
		} else if(temp1.type == JSON_TYPE_INT) {
			chip = json_int(&temp1);
			if (!gpiochip_get_dev(chip, &pin->chip_major, &pin->chip_minor))
				return 0;
			snprintf(pin->device, 255, "/dev/gpiochip%d", chip);
			pin->device[255] = 0;
		}
		return 1;
	} else if (json_value_isequal(json_start, json_len, "hint", "device")) {
		if (!json_get_value(json_start, json_len, "pin", &temp1))
			return 0;

		pin->pin = json_int(&temp1);

		s = json_value_strdup(json_start, json_len, "device");
		if (s == NULL)
			return 0;

		if (!parse_device(&pin->chip_major, &pin->chip_minor, s)) {
			free(s);
			return 0;
		}
		strncpy(pin->device, s, 255);
		pin->device[255] = 0;
		free(s);
		return 1;
	} else if (json_value_isequal(json_start, json_len, "hint", "of")) {
		if (!json_get_value(json_start, json_len, "pin", &temp1))
			return 0;

		pin->pin = json_int(&temp1);

		if (!json_get_value(json_start, json_len, "of", &hint_value) || (hint_value.type != JSON_TYPE_ARRAY) || !hint_value.count)
			return 0;

		gpiochip_of = malloc(sizeof(struct gpiochip_of_struct) * hint_value.count);
		if (gpiochip_of == NULL)
			return 0;

		for (i = 0; i < hint_value.count; i++)
			gpiochip_of[i].compatible = NULL;

		ret = 0;
		for (i = 0; i < hint_value.count; i++) {
			if (!json_get(hint_value.start, hint_value.len, i, NULL, &temp1) || (temp1.type != JSON_TYPE_MAP))
				break;

			if (!json_get_value(temp1.start, temp1.len, "reg", &temp2))
				break;

			gpiochip_of[i].reg = json_int(&temp2);

			gpiochip_of[i].compatible = json_value_strdup(temp1.start, temp1.len, "compatible");
			if (gpiochip_of[i].compatible == NULL)
				break;
		}
		if (i == hint_value.count) {
			chip = gpiochip_of_get(hint_value.count, gpiochip_of);
			if (chip >= 0) {
				if (gpiochip_get_dev(chip, &pin->chip_major, &pin->chip_minor))
					ret = 1;
			}
		}
		for (i = 0; i < hint_value.count; i++) {
			if (gpiochip_of[i].compatible != NULL)
				free(gpiochip_of[i].compatible);
		}
		free(gpiochip_of);
		snprintf(pin->device, 255, "/dev/gpiochip%d", chip);
		pin->device[255] = 0;
		return ret;
	}
	return 0;
}

static int parse_device(int *major, int *minor, char *device)
{
	struct stat s;

	if (stat(device, &s) < 0)
		return 0;

	if ((s.st_mode & S_IFMT) != S_IFCHR)
		return 0;

	*major = major(s.st_rdev);
	*minor = minor(s.st_rdev);

	return 1;
}

