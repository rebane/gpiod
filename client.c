#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include "json.h"
#include "socket_ud.h"
#include "config.h"

static int set(char *socket_path, void *data, int count);

int main(int argc, char *argv[])
{
	int c;
	char *type, *alias, *name, *chip, *of, *pin, *value;
	char *orphan1, *orphan2;
	char *etc_path, *socket_path;
	char *data;

	type = alias = name = chip = of = pin = value = NULL;
	orphan1 = orphan2 = NULL;
	etc_path = "/etc/gpiod.d";
	socket_path = "/tmp";
	data = NULL;

	opterr = 0;

	while ((c = getopt(argc, argv, "-he:s:t:a:n:g:o:l:")) != -1) {
		if (c == 'h') {
			printf("HELP\n");
		} else if (c == 'e') {
			etc_path = optarg;
		} else if (c == 's') {
			socket_path = optarg;
		} else if (c == 't') {
			type = optarg;
		} else if (c == 'a') {
			alias = optarg;
		} else if (c == 'n') {
			name = optarg;
		} else if (c == 'g') {
			chip = optarg;
		} else if (c == 'o') {
			of = optarg;
		} else if (c == 'l') {
			pin = optarg;
		} else if (c == '\1') {
			if (orphan1 == NULL) {
				orphan1 = optarg;
			} else if (orphan2 == NULL) {
				orphan2 = optarg;
			}
		} else {
			printf("ERROR\n");
		}
	}

	if (orphan1 == NULL) {
		fprintf(stderr, "missing value\n");
		return 1;
	}

	if (orphan2 == NULL) {
		value = orphan1;
	} else {
		alias = orphan1;
		value = orphan2;
	}

	if (alias != NULL) {
		data = config_find_alias(etc_path, alias, value);
	} else {
		data = NULL;
//		data = data_build(type, name, chip, of, pin, value);
	}
	if (data == NULL) {
		fprintf(stderr, "error\n");
		return 1;
	}

//	printf("%s\n", data);
//	printf("%s, %s\n", orphan1, orphan2);
	set(socket_path, data, strlen(data));

	return 0;
}

static int set(char *socket_path, void *data, int count)
{
	char buffer[65536];
	char *local_socket, *remote_socket;
	int fd;

	if (asprintf(&local_socket, "%s/gpiod-%d", socket_path, (int)getpid()) < 0)
		return -1;

	if (asprintf(&remote_socket, "%s/gpiod-socket", socket_path) < 0) {
		free(local_socket);
		return -1;
	}

	fd = socket_ud_bind(local_socket);
	if (fd < 0) {
		unlink(local_socket);
		free(local_socket);
		free(remote_socket);
		fprintf(stderr, "socket\n");
		return -1;
	}
	if (socket_ud_sendto(fd, 0, data, count, remote_socket) < 0) {
		socket_ud_close(fd);
		unlink(local_socket);
		free(local_socket);
		free(remote_socket);
		fprintf(stderr, "send\n");
		return -1;
	}

	socket_ud_recvfrom(fd, 0, buffer, 65536, NULL);
	socket_ud_close(fd);
	unlink(local_socket);
	free(local_socket);
	free(remote_socket);
	return 0;
}

