#ifndef _PARSE_MODE_H_
#define _PARSE_MODE_H_

#include "gpiod.h"

int parse_mode(struct gpiod_mode_struct *mode, unsigned char *json_start, int json_len);

#endif

