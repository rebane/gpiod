#include "gpiochip.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/gpio.h>
#include <linux/types.h>
#include "gpiod.h"

int gpiochip_open(struct gpiod_chip_struct *c, int pin)
{
	struct gpio_v2_line_config config;
	struct gpio_v2_line_request req;
	int fd;

	fd = open(c->gpio[pin].pin.device, 0);
	if (fd < 0)
		return 0;

	memset(&req, 0, sizeof(req));
	req.offsets[0] = pin;
	req.num_lines = 1;
	strcpy(req.consumer, "gpiod");
	memset(&config, 0, sizeof(config));
	if (c->gpio[pin].mode.dir == GPIOD_MODE_DIR_OUTPUT)
		config.flags = GPIO_V2_LINE_FLAG_OUTPUT;
	req.config = config;
	if (ioctl(fd, GPIO_V2_GET_LINE_IOCTL, &req) < 0) {
		close(fd);
		return 0;
	}
	close(fd);
	c->gpio[pin].pin.fd = req.fd;

	return 1;
}

int gpiochip_set(struct gpiod_chip_struct *c, int pin)
{
	struct gpio_v2_line_values values;

	if (c->gpio[pin].mode.dir == GPIOD_MODE_DIR_OUTPUT) {
		values.mask = 1;
		if (c->gpio[pin].value == GPIOD_VALUE_TRUE) {
			values.bits = 1;
		} else {
			values.bits = 0;
		}
		if (ioctl(c->gpio[pin].pin.fd, GPIO_V2_LINE_SET_VALUES_IOCTL, &values) < 0)
			return 0;
		return 1;
	}
	return 0;
}

int gpiochip_get_dev(int gpiochip, int *major, int *minor)
{
	char buffer[1024];
	int f, l;

	l = 0;
	l += snprintf(&buffer[l], 1024 - l, "/sys/bus/gpio/devices/gpiochip%d/dev", gpiochip);
	if(l > 1023)
		l = 1023;
	buffer[l] = 0;

	f = open(buffer, O_RDONLY);
	if (f < 0)
		return 0;
	l = read(f, buffer, 1023);
	close(f);
	if (l < 0)
		return 0;
	buffer[l] = 0;
	if (sscanf(buffer, "%d:%d", major, minor) != 2)
		return 0;

	return 1;
}

int gpiochip_get_dev_str(char *gpiochip, int *major, int *minor)
{
	char buffer[1024];
	int f, l;

	l = 0;
	l += snprintf(&buffer[l], 1024 - l, "/sys/bus/gpio/devices/%s/dev", gpiochip);
	if(l > 1023)
		l = 1023;
	buffer[l] = 0;

	f = open(buffer, O_RDONLY);
	if (f < 0)
		return 0;
	l = read(f, buffer, 1023);
	close(f);
	if (l < 0)
		return 0;
	buffer[l] = 0;
	if (sscanf(buffer, "%d:%d", major, minor) != 2)
		return 0;

	return 1;
}

