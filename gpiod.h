#ifndef _GPIOD_H_
#define _GPIOD_H_

#define GPIOD_CHIP_COUNT               32
#define GPIOD_GPIO_COUNT               64

#define GPIOD_CHIP_TYPE_UNUSED         0
#define GPIOD_CHIP_TYPE_GPIOCHIP       1
#define GPIOD_CHIP_TYPE_UART           2
#define GPIOD_CHIP_TYPE_PARPORT        3

#define GPIOD_MODE_DIR_UNCHANGED       0
#define GPIOD_MODE_DIR_INPUT           1
#define GPIOD_MODE_DIR_OUTPUT          2

#define GPIOD_MODE_BIAS_UNCHANGED      0
#define GPIOD_MODE_BIAS_NONE           1
#define GPIOD_MODE_BIAS_PULLUP         2
#define GPIOD_MODE_BIAS_PULLDOWN       3

#define GPIOD_MODE_OUTPUT_UNCHANGED    0
#define GPIOD_MODE_OUTPUT_PUSHPULL     1
#define GPIOD_MODE_OUTPUT_OPENDRAIN    2
#define GPIOD_MODE_OUTPUT_OPENSOURCE   3

#define GPIOD_MODE_ACTIVE_UNCHANGED    0
#define GPIOD_MODE_ACTIVE_HIGH         1
#define GPIOD_MODE_ACTIVE_LOW          2

#define GPIOD_MODE_DEBOUNCHE_UNCHANGED 0
#define GPIOD_MODE_DEBOUNCHE_OFF       1
#define GPIOD_MODE_DEBOUNCHE_ON        2

#define GPIOD_MODE_SCHMITT_UNCHANGED   0
#define GPIOD_MODE_SCHMITT_OFF         1
#define GPIOD_MODE_SCHMITT_ON          2

#define GPIOD_VALUE_UNCHANGED          0
#define GPIOD_VALUE_FALSE              1
#define GPIOD_VALUE_TRUE               2

struct gpiod_pin_struct {
	int chip_type;
	int chip_major;
	int chip_minor;
	char device[256];
	int fd;
	int pin;
};

struct gpiod_mode_struct {
	int dir;
	int bias;
	int output;
	int active;
	int debounche;
	int schmitt;
};

struct gpiod_gpio_struct {
	struct gpiod_pin_struct pin;
	struct gpiod_mode_struct mode;
	int value;
};

struct gpiod_chip_struct {
	int type;
	int major;
	int minor;
	int fd;
	struct gpiod_gpio_struct gpio[GPIOD_GPIO_COUNT];
};

void gpiod_init();
int gpiod_set(struct gpiod_gpio_struct *gpio);

#endif

