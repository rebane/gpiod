#include "gpiod.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "gpiochip.h"

static struct gpiod_chip_struct gpiod_chip[GPIOD_CHIP_COUNT];

static int gpiod_add(struct gpiod_gpio_struct *g);
static int gpiod_open(int chip, int pin);

void gpiod_init()
{
	int chip, gpio;

	for (chip = 0; chip < GPIOD_CHIP_COUNT; chip++) {
		gpiod_chip[chip].type = GPIOD_CHIP_TYPE_UNUSED;
		for (gpio = 0; gpio < GPIOD_GPIO_COUNT; gpio++) {
			gpiod_chip[chip].gpio[gpio].pin.chip_type = GPIOD_CHIP_TYPE_UNUSED;
		}
	}
}

int gpiod_set(struct gpiod_gpio_struct *g)
{
	int chip;

	chip = gpiod_add(g);
	if (chip < 0)
		return 0;

	printf("========\n");
	printf("TYPE: %d\n", g->pin.chip_type);
	printf("MAJOR: %d\n", g->pin.chip_major);
	printf("MINOR: %d\n", g->pin.chip_minor);
	printf("DEVICE: %s\n", g->pin.device);
	printf("PIN: %d\n", g->pin.pin);
	printf("DIR: %d\n", g->mode.dir);
	printf("BIAS: %d\n", g->mode.bias);
	printf("OUTPUT: %d\n", g->mode.output);
	printf("ACTIVE: %d\n", g->mode.active);
	printf("DEBOUNCHE: %d\n", g->mode.debounche);
	printf("SCHMITT: %d\n", g->mode.schmitt);
	printf("VALUE: %d\n", g->value);
	printf("========\n");

	if (gpiod_chip[chip].type == GPIOD_CHIP_TYPE_GPIOCHIP) {
		return gpiochip_set(&gpiod_chip[chip], g->pin.pin);
	}

	return 0;
}

static int gpiod_add(struct gpiod_gpio_struct *g)
{
	int chip, gpio;

	if (g->pin.pin >= GPIOD_GPIO_COUNT)
		return -1;

	for (chip = 0; chip < GPIOD_CHIP_COUNT; chip++) {
		if ((gpiod_chip[chip].type == g->pin.chip_type) && (gpiod_chip[chip].major == g->pin.chip_major) && (gpiod_chip[chip].minor == g->pin.chip_minor))
			break;
	}

	if (chip == GPIOD_CHIP_COUNT) {
		for (chip = 0; chip < GPIOD_CHIP_COUNT; chip++) {
			if (gpiod_chip[chip].type == GPIOD_CHIP_TYPE_UNUSED) {
				gpiod_chip[chip].type = g->pin.chip_type;
				gpiod_chip[chip].major = g->pin.chip_major;
				gpiod_chip[chip].minor = g->pin.chip_minor;
				gpiod_chip[chip].gpio[g->pin.pin] = *g;
				if (!gpiod_open(chip, g->pin.pin)) {
					gpiod_chip[chip].gpio[g->pin.pin].pin.chip_type = GPIOD_CHIP_TYPE_UNUSED;
					gpiod_chip[chip].type = GPIOD_CHIP_TYPE_UNUSED;
					return -1;
				}
				break;
			}
		}
	} else {
		if (gpiod_chip[chip].gpio[g->pin.pin].pin.chip_type == GPIOD_CHIP_TYPE_UNUSED) {
			gpiod_chip[chip].gpio[g->pin.pin] = *g;
			if (!gpiod_open(chip, g->pin.pin)) {
				gpiod_chip[chip].gpio[g->pin.pin].pin.chip_type = GPIOD_CHIP_TYPE_UNUSED;
				gpiod_chip[chip].type = GPIOD_CHIP_TYPE_UNUSED;
				return -1;
			}
		} else {
			gpiod_chip[chip].gpio[g->pin.pin].mode = g->mode;
			gpiod_chip[chip].gpio[g->pin.pin].value = g->value;
		}
	}

	return chip;
}

static int gpiod_open(int chip, int pin)
{
	if (gpiod_chip[chip].type == GPIOD_CHIP_TYPE_GPIOCHIP) {
		return gpiochip_open(&gpiod_chip[chip], pin);
	}

	return 0;
}

