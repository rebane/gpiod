#include <stdio.h>
#include <stdlib.h>
#include "gpiod_of.h"

// gpiodset usb_power high
// gpiodset -n PA0 low
// gpiodset -o nuvoton,nuc980-spi:0xb0062000/fairchild,74hc595:1 -l 5 high
// gpiodset -g 0 -l 5 low
// -c bias_pullup
// -c bias_pulldown
// -c bias_off
// -c input
// -c output_opendrain
// -c output_opensource
// -c output_pushpull
// -c active_low
// -c active_high
// -c debounche_on
// -c debounche_off
// -c schmitt_on
// -c schmitt_off

// -e etc_dir
// -s socket_dir

// 5,2,output_opendrain,high

/*
				{"type":"gpio","hint":"name","name":"PA5"},
				{"type":"gpio","hint":"gpiochip","gpiochip":"gpiochip1","pin":15},
				{"type":"gpio","hint":"device","device":"/dev/gpiochip1","pin":15},
				{"type":"parport","hint":"device","device":"/dev/parport0","signal":"d0"}},
				{"type":"uart","hint""data":{"device":"/dev/ttyS2","signal":"rts"}}
				usb_power,of,nuvoton,nuc980-spi:0xb0062000/,15

			],
{
	"command":"set",
	"transaction_id":1234562,
	"data":{
		"set":[
			{
				"gpios":[{
					"type":"gpio",
					"hint":"of",
					"of":[{"compatible":"nuvoton,nuc980-spi","reg":0xb0062000}],
					"pin":15
				}],
				"mode":{
					"direction":"output",
					"bias":"pullup",
					"output":"openrain",
					"active":"low",
					"debounche":true,
					"schmitt":true,
				},
				"value":true
			}
		]
	}
	"command":"get",
	"data":{
		"get":[
			{
				"gpio": {}
			}
		]
	}
	"command":"release",
	"data":{
		"release":[
			{
				"gpio": {}
			},
			{
				"gpiochip": "gpiochip15"
			},
			{
				"parport": "/dev/parport0"
			},
		]
	}
}
*/

int main(int argc, char *argv[])
{
	printf("gpiochip: %d\n", gpiod_of_get_arg("nuvoton,nuc980-pinctrl", 0xb0000070, "nuvoton,nuc980-gpio", 0xb0004180, NULL));

	return 0;
}
/*
	snprintf(bind_buf, 256, "/tmp/netd-%d", (int)getpid());
	s = socket_ud_bind(bind_buf);
	if(s < 0){
		unlink(bind_buf);
		fprintf(stderr, "socket\n");
		return(1);
	}
	if(socket_ud_sendto(s, 0, json, l, "/tmp/netd-socket") < 0){
		fprintf(stderr, "send\n");
		socket_ud_close(s);
		unlink(bind_buf);
		return(1);
	}

	socket_ud_recvfrom(s, 0, json, 65536, NULL);
	socket_ud_close(s);
	unlink(bind_buf);
	return(0);
}
*/
