#ifndef _GPIOCHIP_H_
#define _GPIOCHIP_H_

#include "gpiod.h"

int gpiochip_open(struct gpiod_chip_struct *c, int pin);
int gpiochip_set(struct gpiod_chip_struct *c, int pin);

int gpiochip_get_dev(int gpiochip, int *major, int *minor);
int gpiochip_get_dev_str(char *gpiochip, int *major, int *minor);

#endif

